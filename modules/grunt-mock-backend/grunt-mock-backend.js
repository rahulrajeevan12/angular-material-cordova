(function (module) {
  'use strict';

  var mock = require('./lib/mock-backend');
  module.exports = function (grunt) {
    grunt.registerTask('mock-backend', function () {
      mock.mockBackend();
    });
  };
})(module);