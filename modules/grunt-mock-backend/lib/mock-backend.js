(function () {
  'use strict';

  module.exports = {
    mockBackend: function () {

      var jf = require('jsonfile');
      var util = require('util');

      var express = require('express');
      var app = express();
      var bodyParser = require('body-parser');

      app.use(bodyParser.urlencoded({extended: true}));
      app.use(bodyParser.json());

      var port = process.env.PORT || 8078;

      var router = express.Router();

      var path = require('path');
      var dataPath = path.resolve(__dirname + '/../data');

      router.get('/get/menu', function (req, res) {
        jf.readFile(dataPath + '/menu-data.json', function (err, obj) {
          res.json(obj);
        });
      });

      router.get('/get/form/fields', function (req, res) {
        jf.readFile(dataPath + '/transaction-form-fields-data.json', function (err, obj) {
          res.json(obj);
        });
      });

      router.get('/get/rainsummery', function (req, res) {
        jf.readFile(dataPath + '/rain-summery.json', function (err, obj) {
          res.json(obj);
        });
      });
      router.post('/get/login', function (req, res) {
        jf.readFile(dataPath + '/login.json', function (err, obj) {
          res.json(obj);
        });
      });

      router.get('/get/safexPrice', function (req, res) {
        jf.readFile(dataPath + '/SAFEX-prices.json', function (err, obj) {
          res.json(obj);
        });
      });
      router.get('/get/notifications', function (req, res) {
        jf.readFile(dataPath + '/notificationData.json', function (err, obj) {
          res.json(obj);
        });
      });

      app.all('/*', function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, auth-key");
        res.header("Access-Control-Allow-Methods", "GET, POST, PUT");
        next();
      });

      app.use('/api', router);

      app.listen(port);
      app.listen(3000);
      console.log('Magic happens on port ' + port);
    }
  };
})();