module.exports = function (grunt) {
  'use strict';

  var pkg = grunt.file.readJSON('package.json');
  for (var taskName in pkg.devDependencies) {
    if (taskName.indexOf('grunt-') > -1) {
      grunt.loadNpmTasks(taskName);
    }
  }
  grunt.task.loadTasks('./modules/grunt-mock-backend');

  var config = require('config');
  var dependencies = config.get('dependencies');
  var portConfig = config.get('portConfig');

  var _ = require('lodash');
  var devDependencies = _.union(dependencies.vendor, dependencies.css, dependencies.js);

//  var util = require('util');
//  console.log(util.inspect(devDependencies));

  grunt.initConfig({
    pkg: pkg,
    clean: {
      build: ['./.tmp', './www'],
      test: ['./reports', './coverage']
    },
    connect: {
      options: {
        hostname: '0.0.0.0'
      },
      dev: {
        options: {
          port: portConfig.dev,
          livereload: portConfig.livereload,
          base: 'src/app',
          open: {
            target: 'http://localhost:<%= connect.dev.options.port %>/index.html'
          }
        }
      },
      build: {
        options: {
          keepalive: true,
          port: portConfig.build,
          base: 'www',
          open: {
            target: 'http://localhost:<%= connect.build.options.port %>/index.html'
          }
        }
      }
    },
    sass: {
      dev: {
        options: {
          sourcemap: 'none',
          style: 'expanded',
          trace: true
        },
        files: {
          // target.css file: source.scss file
          'src/app/assets/css/app.css': 'src/app/assets/sass/app.scss'
        }
      }
    },
    injector: {
      options: {
        relative: true,
        addRootSlash: false
      },
      devDependencies: {
        files: {
          'src/app/index.html': devDependencies
        }
      },
      prodDependencies: {
        files: {
          'www/index.html': [
            'www/css/app.css',
            'www/scripts/app.js'
          ]
        }
      }
    },
    watch: {
      options: {
        spawn: false
      },
      styles: {
        files: [
          'src/app/assets/sass/**/*.scss'
        ],
        tasks: ['sass:dev']
      },
      sources: {
        options: {
          livereload: portConfig.livereload
        },
        files: [
          'src/app/scripts/**/*.js',
          'src/app/css/**/*.css',
          'src/app/*.html'
        ]
      }
    },
    copy: {
      build: {
        files: [
          {expand: true, cwd: 'src/app/', src: ['index.html', 'scripts/modules/**/*.html','scripts/common/**/*.html'], dest: 'www/'},
          {expand: true, cwd: 'src/app/assets/', src: ['images/**'], dest: 'www/'},
          {expand: true, cwd: 'src/app/vendor/ionic/', src: ['fonts/**'], dest: 'www/'}
        ]
      }
    },
    useminPrepare: {
      options: {
        dest: 'www'
      },
      html: 'src/app/index.html'
    },
    karma: {
      unit: {
        configFile: 'src/test/karma-unit.conf.js',
        autoWatch: false,
        singleRun: true
      }
    }
  });

  // Development
  grunt.registerTask('default', ['mock-backend', 'injector:devDependencies', 'sass', 'connect:dev', 'watch']);
  
  // Unit test
  grunt.registerTask('unitTest', ['clean:test', 'karma:unit']);

  // Build
  grunt.registerTask('build', [
    'clean:build',
    'useminPrepare',
    'concat:generated',
    'cssmin:generated',
    'uglify:generated',
    'copy:build',
    'injector:prodDependencies',
    'connect:build'
  ]);
};