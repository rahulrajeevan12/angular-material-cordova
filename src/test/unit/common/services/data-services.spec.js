describe("Testing 'dataStore' service", function () {
  beforeEach(module('ionicCordova'));

  it('There must be a service named "dataStore" service', inject(function (dataStore) {
    expect(dataStore).not.to.equal(null);
  }));

  it('"dataStore.getNewStore" should return a new object', inject(function (dataStore) {
    expect(dataStore.getNewStore('test')).to.be.a('object');
  }));
  it('"dataStore.getStore" should return an object, if the object is already present in the same tag',
          inject(function (dataStore) {
            var test = dataStore.getNewStore('test');
            test['test'] = 'test';
            expect(dataStore.getStore('test')).to.have.property('test').to.equal('test');
          }));
  it('"dataStore.deleteStore" should delete an object, if the object is already present',
          inject(function (dataStore) {
            var test = dataStore.getNewStore('test');
            test['test'] = 'test';
            expect(dataStore.getStore('test')).to.have.property('test').to.equal('test');
            dataStore.deleteStore('test');
            expect(dataStore.getStore('test')).to.be.undefined;
          }));
});
