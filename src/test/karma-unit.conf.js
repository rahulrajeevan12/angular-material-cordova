module.exports = function (configuration) {

  var _ = require('lodash');
  var config = require('config');
  var dependencies = config.get('dependencies');
  
  var requiredFiles = _.union ([
    'src/main/vendor/angular/angular.js',
    'src/main/vendor/angular-resource/angular-resource.js',
    'src/main/vendor/angular-cookies/angular-cookies.js',
    'src/main/vendor/angular-animate/angular-animate.js',
    'src/main/vendor/angular-sanitize/angular-sanitize.js',
    'src/main/vendor/angular-ui-router/release/angular-ui-router.js',
    'src/main/vendor/ionic/js/ionic.js',
    'src/main/vendor/ionic/js/ionic-angular.js',
    'src/main/vendor/ngCordova/dist/ng-cordova.js',
    'src/main/vendor/angular-mocks/angular-mocks.js',
    // Lib files
    'node_modules/chai/chai.js',
    'src/test/lib/*.js',
    // Test specs
    'src/test/unit/**/*.spec.js'
  ], dependencies.js);

  var options = {
    basePath: '../../',
    frameworks: ['mocha'],
    reporters: ['progress', 'html', 'coverage'],
    browsers: ['Chrome'],
    autoWatch: true,
    module: {
      name: 'ngCordova-poc-Testing'
    },
    // these are default values anyway
    singleRun: false,
    colors: true,
    files: requiredFiles,
    // the default configuration
    htmlReporter: {
      outputDir: 'reports'
    },
    preprocessors: {
      'src/main/common/**/*.js': ['coverage'],
      'src/main/modules/**/*.js': ['coverage'],
      'src/main/app.js': ['coverage'],
      'src/main/common/**/*.html': 'html2js',
      'src/main/modules/**/*.html': 'html2js'
    },
    exclude: [
    ],
    ngHtml2JsPreprocessor: {
      // strip app from the file path
      stripPrefix: 'src/main/'
    },
    coverageReporter: {
      type: 'html',
      dir: 'coverage/'
    }
  };

  configuration.set(options);
};