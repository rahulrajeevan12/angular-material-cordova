(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .constant('appConstants', {
            errorMessageCodes: [
              'generic.error'
            ]

          });
})();