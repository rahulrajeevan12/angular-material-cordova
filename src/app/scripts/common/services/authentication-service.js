(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .service('authentication', ['dataClient', 'dataStore', '$state',
            function (dataClient, dataStore, $state) {

              var user = {};

              var authService = {
                isAuthorized: function () {
                  if (user && !user['Auth-Key'])
                    user = dataStore.getStore('user');
                  return user ? user['Auth-Key'] : false;
                },
                doLogOut: function () {
                  user = {};
                  return dataStore.deleteStore('user');
                },
                authenticateLogin: function (credential, callback) {
                  if (!(credential && credential.email && credential.password))
                    return false;
                  var login = dataClient.getData('getLoginDetails', {method: 'POST', data: {email: credential.email, password: credential.password}});
                  login.then(
                          function (data) {
                            var authKey = data['auth-key'],
                                    flag = false;
                            if (authKey) {
                              flag = true;
                              user = dataStore.getNewStore('user');
                              user['Auth-Key'] = data['auth-key'];
                              user['user'] = data['user'];
                              dataStore.putStore('user');
                            }
                            else {
                              $state.go('login');
                            }
                            if (callback)
                              callback(flag);
                          },
                          function (error) {
                            //$log.error('Failed' + error);
                            if (callback)
                              callback(false);
                          }
                  );
                }
              };

              return authService;
            }]);
})();