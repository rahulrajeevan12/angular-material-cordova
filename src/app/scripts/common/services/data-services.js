(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .service('dataStore', ['$cookieStore', function ($cookieStore) {

              var store = {};
              var dataService = {
                //Allocates a new store
                getNewStore: function (key) {
                  if (!store[key])
                    store[key] = {};
                  return store[key];
                },
                //Stores the cookie value
                putStore: function (key) {
                  if (store[key]) {
                    $cookieStore.put(key, store[key]);
                  }
                },
                //Fetches the stored cookie value
                getStore: function (key) {
                  if (!store[key]) {
                    store[key] = $cookieStore.get(key);
                  }
                  return store[key];
                },
                //Deletes the stored cookie
                deleteStore: function (key) {
                  if (store[key])
                    delete store[key];
                  $cookieStore.remove(key);
                }
              };

              return dataService;
            }]);
})();