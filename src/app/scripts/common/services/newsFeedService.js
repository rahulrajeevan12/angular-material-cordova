(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .service('newsFeedService', ['$http', '$log', function ($http) {
              this.loadBlogs = function (date) {
                var params = {number: 15};
                if (date)
                  params.before = date;
                return ($http.get("https://public-api.wordpress.com/rest/v1/freshly-pressed/", {
                  params: params
                }));
              };
            }]);
})();