(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .constant('dataConstants', {
            getLoginDetails: {
              url: '/get/login'
            },
            getMenu: {
              url: '/get/menu'
            },
            getFormFields: {
              url: '/get/form/fields'
            },
            getNotificationList: {
              url: '/get/notifications'
            },
            getSafexPrice: {
              url: '/get/safexPrice'
            },
            getRainSummery: {
              url: '/get/rainSummery'
            }
          })
          .provider('dataClient', [
            function () {
              var config = {
                endPoint: ''
              };
              this.setConfig = function (newConfig) {
                angular.extend(config, newConfig);
                config.endPoint = angular.isUndefined(config.endPoint) ? '' : config.endPoint;
              };

              // Request queue
              function Queue() {
                var queue = [];
                this.push = function (req) {
                  return queue.push(req);
                };
                this.length = function () {
                  return queue.length;
                };
                this.execNext = function ($http) {
                  var task = queue[0];
                  $http(task.c).then(function (res) {
                    queue.shift();

                    task.d.resolve(res.data);

                    if (queue.length > 0) {
                      execNext($http);
                    }
                  }, function (err) {
                    queue.shift();

                    task.d.reject(err);

                    if (queue.length > 0) {
                      execNext();
                    }
                  });
                };
              }

              // Queue of requests 			 
              var queue = new Queue();
              this.$get = ['$http', '$q', '$timeout', 'dataConstants',
                function ($http, $q, $timeout, dataConstants) {

                  var dataClient = {getData: function (actionName, options) {
                      if (!options) {
                        options = {};
                      }
                      var url = dataConstants[actionName] ?
                              dataConstants[actionName].url : null;
                      var d = $q.defer();
                      if (url === null) {
                        var msg = 'Action: "' + actionName + '" is not defined';
                        $timeout(function () {
                          d.reject(msg);
                        });
                        return d;
                      }
                      else {
                        url = config.endPoint + url;
                        queue.push({
                          c: {
                            method: options.method || 'GET',
                            data: options.data || {}, url: url
                          },
                          d: d
                        });
                        if (queue.length() === 1) {
                          queue.execNext($http);
                        }
                        return d.promise;
                      }
                    }
                  };
                  return dataClient;
                }];
            }]);
})();