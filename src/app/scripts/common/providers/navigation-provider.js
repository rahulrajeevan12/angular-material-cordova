(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .provider('navigate', function () {
            var nav = {};
            nav.navRules = {};

            angular.extend(this, {
              setRules: function (rules) {
                angular.forEach(rules, function (value, key) {
                  nav.navRules[key] = value;
                });
              },
              $get: ['$location', function ($location) {
                  //var translate = $filter('translate');
                  var navProvider = {
                    //Checks if the route is defined
                    isDefinedRoute: function (route) {
                      var route_m = /\/$/.test(route)
                              ? route.substring(0, route.length - 1)
                              : route,
                              isDefined = false,
                              definedRoutes = nav.navRules['definedRoutes'];
                      route_m = (route_m.indexOf("#") > -1) ? route_m.split('#')[1] : route_m;
                      angular.forEach(definedRoutes, function (value) {
                        var p = route_m.indexOf(value);
                        if (!isDefined && p > -1) {
                          isDefined = true;
                        }
                      });
                      return isDefined;
                    },
                    //Checks if the route requires authentication
                    isAuthRequiredRoute: function (route) {
                      if (!navProvider.isDefinedRoute(route))
                        return false;

                      var route_m = /\/$/.test(route)
                              ? route.substring(0, route.length - 1)
                              : route,
                              isAuthRequired = true,
                              nonAuthRoutes = nav.navRules['noAuthRequiredRoutes'];
                      route_m = (route_m.indexOf("#") > -1) ? route_m.split('#')[1] : route_m;
                      angular.forEach(nonAuthRoutes, function (value) {
                        var p = route_m.indexOf(value);
                        if (isAuthRequired && p > -1) {
                          isAuthRequired = false;
                        }
                      });
                      return isAuthRequired;
                    },
                    //Navigates to a particular destination
                    go: function (dest) {
                      if (navProvider.isDefinedRoute(dest)) {
                        $location.path(dest);
                      }
                      else
                        console.log('pageNotImplemented');
                      //notification.error(dest + ' ' + translate('pageNotImplemented'), true);
                    },
                    //Navigates to a preset destination
                    goToPreset: function (dest) {
                      navProvider.go(nav.navRules.presets[dest]);
                    }
                  };

                  return navProvider;
                }]
            });
          });
})();