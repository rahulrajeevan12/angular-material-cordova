(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .provider('notification', function () {
            angular.extend(this, {
              $get: ['$mdDialog', function ($mdDialog) {
                  var notification = {
                    notice: function (content) {
                      return this.notify(content, 'button-assertive');
                    },
                    info: function (content) {
                      return this.notify(content, 'button-energized');
                    },
                    success: function (content) {
                      return this.notify(content, 'button-assertive');
                    },
                    error: function (content) {
                      return this.notify(content, 'button-assertive');
                    },
                    notify: function (content) {
                      $mdDialog.show(
                              $mdDialog.alert()
                              .parent(angular.element(document.body))
                              .content(content)
                              .ok('Ok')
                              );
                    }

                  };
                  return notification;
                }]
            });
          });

})();