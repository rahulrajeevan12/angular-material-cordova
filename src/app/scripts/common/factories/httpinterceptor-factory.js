(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .factory('httpInterceptor', ['$q', 'dataStore', '$location',
            function ($q, dataStore, $location) {
              var interceptor = {
                isAllowed: function (url) {
                  return (/(.html|.js|.json|.map|.png|.jpg|.jpeg|\/get\/login)$/.test(url));
                },
                request: function (request) {
                  var url = request.url;

                  if (angular.isUndefined(url))
                    return $q.reject(request);
                  else if (!interceptor.isAllowed(url)) {
                    var user = dataStore.getStore('user');
                    if (user && user['Auth-Key']) {
                      request.headers['Auth-Key'] = user['Auth-Key'];
                    }
                    else {
                      return $q.reject(request);
                    }
                  }
                  return request || $q.when(request);
                },
                requestError: function (rejection) {
                  return $q.reject(rejection);
                },
                response: function (response) {
                  if (!response.data.success && !interceptor.isAllowed(response.config.url)) {
                    debugger;
//							var errormsg = response.data.errormsg;
                    var user = dataStore.getStore('user');
                    if (user && user['Auth-Key']) {
                      dataStore.deleteStore('user');
                      $location.path('/login');
                    }
//							if (angular.isUndefined(errormsg) ||
//									!errormsg in appConstants.errorMessageCodes)
//								errormsg = 'generic.error';
//							response.data = {
//								errormsg: errormsg,
//								success: false
//							};
                    return $q.reject(response);
                  }

                  return response || $q.when(response);
                },
                responseError: function (rejection) {
                  rejection.data = {
                    errormsg: 'generic.error',
                    success: false
                  };
                  return $q.reject(rejection);
                }
              };

              return interceptor;
            }]);
})();