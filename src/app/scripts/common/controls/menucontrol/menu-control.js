(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .directive('navMenu', ['authentication', '$state', function (authentication, $state) {
              return {
                restrict: 'AE',
                replace: true,
                templateUrl: 'scripts/common/controls/menucontrol/menucontrol.html',
                link: function ($scope) {
                  $scope.menus = [
                    {name: 'DAILY REPORT', url: 'newsFeed'},
                    {name: 'NOTIFICATIONS', url: 'notifications'},
                    {name: 'WEATHER', url: 'weather'},
                    {name: 'GRAPHS', url: 'graphs'},
                    {name: 'BUY/SELL', url: 'buysell'},
                    {name: 'Angular Material', url: 'material'}

                  ];
                  $scope.doLogOut = function () {
                    authentication.doLogOut();
                    $state.go('login');
                  };
                }
              };

            }]);
})();