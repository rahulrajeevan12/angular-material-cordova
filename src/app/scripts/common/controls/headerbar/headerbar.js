(function () {
  'use strict';
  angular.module('ionicCordova.common')
          .directive('headerBar', ['$mdBottomSheet', '$mdSidenav', '$q', function ($mdBottomSheet, $mdSidenav, $q) {
              return {
                restrict: 'AE',
                replace: true,
                scope: {
                  title: "=headerTitle"
                },
                templateUrl: 'scripts/common/controls/headerbar/headerbar.html',
                link: function ($scope) {
                  $scope.toggleList = function () {
                    var pending = $mdBottomSheet.hide() || $q.when(true);

                    pending.then(function () {
                      $mdSidenav('left').toggle();
                    });
                  };
                }
              };

            }]);
})();