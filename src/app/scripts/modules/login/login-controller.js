(function () {
  'use strict';
  angular.module('ionicCordova.modules')
          .controller('loginCtrl', ['$scope', '$state', 'authentication','notification',
            function ($scope, $state, authentication,notification) {
              $scope.loginData = {
                email: '',
                password: ''
              };
              $scope.authSuccess = true;
              $scope.doLogin = function () {
                if ($scope.loginData.email.length <= 0 && $scope.loginData.password.length <= 0) {
                  notification.info('Enter a valid emailID and password');
                }
                else if ($scope.loginData.email.length <= 0) {
                  notification.info('Enter EmailId');
                }
                else if ($scope.loginData.password.length <= 0) {
                  notification.error('Enter Password');
                }
                else {
                  authentication.authenticateLogin($scope.loginData, function (status) {
                    if (status) {
                      $state.go('buysell');
                    }
                    else {
                      notification.error('Invalid Username or password', 'Error');
                    }
                  });
                }


              };

            }]);
})();