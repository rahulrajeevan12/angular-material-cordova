(function () {
  'use strict';
  angular.module('ionicCordova.modules')
          .controller('buySellCtrl', ['$scope', 'dataClient', '$state', '$mdBottomSheet',
            function ($scope, dataClient, $state, $mdBottomSheet) {
              $scope.buyVisibility = false;
              $scope.$parent.headerTitle = "Buy Sell"
              var dataPromise = dataClient.getData('getSafexPrice', {method: 'GET'});
              dataPromise.then(function (data) {
                $scope.prices = data.data;
              }, function (reason) {
                $state.go('login');
              });

              $scope.checkStatus = function (change) {
                if (change >= 30)
                  return 'BUY';
                else
                  return 'NEUTRAL';
              };
              $scope.showGridBottomSheet = function ($event) {
                $scope.alert = '';
                $mdBottomSheet.show({
                  templateUrl: 'scripts/modules/buysell/bottom-sheet-grid-template.html',
                  controller: 'GridBottomSheetCtrl',
                  targetEvent: $event
                }).then(function (clickedItem) {
                  $scope.alert = clickedItem.name + ' clicked!';
                });
              };

//              $ionicModal.fromTemplateUrl('modules/buysell/buy.html', {
//                scope: $scope
//              }).then(function (modal) {
//                $scope.modal = modal;
//              });

              $scope.loginData = {};

              $scope.buyStock = function (price) {
                if (price.change >= 30) {
                  $scope.buyVisibility = true;
                }
                else {
                  $scope.buyVisibility = false;
                }
                $scope.modalData = price;
                $scope.modal.show();
              };
              $scope.closeLogin = function () {
                $scope.modal.hide();
              };
            }])
          .controller('GridBottomSheetCtrl', function ($scope, $mdBottomSheet) {
            $scope.items = [
              {name: 'Buy', icon: 'shopping_cart'},
              {name: 'Sell', icon: 'trending_down'},
              {name: 'Cancel', icon: 'cancel'}
            ];

            $scope.listItemClick = function ($index) {
              var clickedItem = $scope.items[$index];
              $mdBottomSheet.hide(clickedItem);
            };
          })

})();
