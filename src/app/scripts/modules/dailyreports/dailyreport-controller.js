(function () {
  'use strict';

  angular.module('ionicCordova.modules')
          .controller('dailyReportCtrl', ['$scope', '$sce',  'newsFeedService',
            function ($scope, $sce, newsFeedService) {
              $scope.$parent.headerTitle = "DAILY REPORT"
              $scope.posts = [];
              $scope.beforeDate = null;
              $scope.infiniteLoad = false;
              $scope.viewBlog = function (url) {
                window.open(url, "_blank", "location=no");
              };
              $scope.loadBlogs = function () {
                $scope.infiniteLoad = false;
                $scope.beforeDate = null;
                if ($scope.posts.length) {
                  $scope.posts = [];
                  $scope.moreBlogs();
                }
                $scope.$broadcast("scroll.refreshComplete");
                $scope.infiniteLoad = true;
              };
              $scope.loadBlogs();
              $scope.moreBlogs = function () {
                $ionicLoading.show({template: "Loading..."});
                newsFeedService.loadBlogs($scope.beforeDate)
                        .success(function (result) {
                          $scope.posts = $scope.posts.concat(result.posts);
                          $scope.beforeDate = result.date_range.oldest;
                          $scope.$broadcast("scroll.infiniteScrollComplete");
                          $ionicLoading.hide();
                        });
              };
              $scope.toTrusted = function (text) {
                return ($sce.trustAsHtml(text));
              };
            }]);
})();
