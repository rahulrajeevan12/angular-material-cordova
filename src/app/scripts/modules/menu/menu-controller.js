(function () {
  'use strict';
  angular.module('ionicCordova.modules')
          .controller('menuCtrl', ['$scope', '$state', 'authentication', 'notification', '$mdSidenav',
            function ($scope, $state, authentication, notification, $mdSidenav) {
              $scope.menus = [
                {name: 'DAILY REPORT', href: '#F'},
                {name: 'NOTIFICATIONS', href: '#F'},
                {name: 'GRAPHS', href: '#F'},
                {name: 'BUY/SELL', href: '#F'},
                {name: 'LOGOUT', href: '#F'}
              ];
              $scope.doLogOut = function () {
                authentication.doLogOut();
                $state.go('noMenu.login');
              };



            }]);
})();