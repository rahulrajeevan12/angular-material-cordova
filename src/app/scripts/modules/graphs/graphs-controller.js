(function () {
  'use strict';
  angular.module('ionicCordova.modules')
          .controller('graphsCtrl', ['$scope', function ($scope) {
              $scope.$parent.headerTitle = "GRAPHS";
            }]);
})();