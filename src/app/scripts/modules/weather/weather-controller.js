(function () {
  'use strict';
  angular.module('ionicCordova.modules')
          .controller('weatherCtrl', ['$scope', 'dataClient',
            function ($scope, dataClient) {
              $scope.$parent.headerTitle = "WEATHER";
              $scope.allStationData = {};
              $scope.bgvDatas = [];
              var dataPromise = dataClient.getData('getRainSummery', {method: 'GET'});
              dataPromise.then(function (data) {
                $scope.allStationData = data;
                $scope.bgvDatas = data.bgv;
//						$scope.weatherDetails = data;
              }, function (reason) {
                alert('Failed due to ' + reason);
              });


              $scope.toggleRainSummery = function (val) {
                if (!val)
                  $scope.bgvDatas = $scope.allStationData.bgv;
                else
                  $scope.bgvDatas = $scope.allStationData.pretoria;
              };

            }]);
})();