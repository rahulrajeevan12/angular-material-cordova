(function () {
  'use strict';
  angular.module('ionicCordova.modules')
          .controller('notificationListCtrl', ['$scope', '$state', 'dataClient', '$mdDialog',
            function ($scope, $state, dataClient, $mdDialog) {
              $scope.$parent.headerTitle = "NOTIFICATIONS"
              var dataPromise = dataClient.getData('getNotificationList', {method: 'GET'});
              dataPromise.then(function (data) {
                $scope.notifications = data.data;
                $scope.$parent.notifications = data.data;
              }, function () {
                $state.go('login');
              });
              $scope.orderProp = 'age';

              $scope.showMore = function (ev) {
                $mdDialog.show({
                  controller: dialogCtrl,
                  templateUrl: 'scripts/modules/notifications/dialogtemplate.html',
                  parent: angular.element(document.body),
                  targetEvent: ev
                })
                        .then(function (answer) {

                        }, function () {

                        });
              };

            }]);

})();