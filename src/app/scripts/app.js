(function () {
  'use strict';

  var modules = ['ui.router', 'ngMaterial', 'ionicCordova.modules', 'ngCookies', 'ionicCordova.common', 'ngMdIcons'];

  angular.module('ionicCordova', modules)
          .config(['$stateProvider', 'dataClientProvider', '$httpProvider', '$urlRouterProvider', '$mdThemingProvider',
            function ($stateProvider, dataClientProvider, $httpProvider, $urlRouterProvider, $mdThemingProvider) {
              $mdThemingProvider.theme('default')
                      .primaryPalette('red')
                      .accentPalette('orange');
              $stateProvider
                      .state('main', {
                        url: "/main",
                        templateUrl: "scripts/modules/landingpage/landingpage.html"
                      })
                      .state('login', {
                        url: "/login",
                        templateUrl: "scripts/modules/login/login.html",
                        controller: 'loginCtrl'
                      })
                      .state('register', {
                        url: "/register",
                        templateUrl: "scripts/modules/signup/signup.html",
                        controller: 'loginCtrl'
                      })
                      .state('newsFeed', {
                        url: "/newsFeed",
                        templateUrl: "scripts/modules/dailyreports/dailyreport.html",
                        controller: 'dailyReportCtrl'
                      })
                      .state('notifications', {
                        url: "/notifications",
                        templateUrl: "scripts/modules/notifications/notifications.html",
                        controller: 'notificationListCtrl'
                      })

                      .state('graphs', {
                        url: "/graphs",
                        templateUrl: "scripts/modules/graphs/graphs.html",
                        controller: 'graphsCtrl'
                      })
                      .state('buysell', {
                        url: "/buysell",
                        templateUrl: "scripts/modules/buysell/buySell.html",
                        controller: 'buySellCtrl'
                      })
                      .state('material', {
                        url: "/material",
                        templateUrl: "scripts/modules/angularmaterial/angularmaterial.html",
                        controller: 'materialCtrl'
                      })
                      .state('weather', {
                        url: "/weather",
                        templateUrl: "scripts/modules/weather/weather.html",
                        controller: 'weatherCtrl'
                      });


              dataClientProvider.setConfig({
                endPoint: 'http://localhost:8078/api'
              });


              $httpProvider.interceptors.push('httpInterceptor');
              $httpProvider.defaults.useXDomain = true;
              delete $httpProvider.defaults.headers.common['X-Requested-With'];


              $urlRouterProvider.otherwise('main');
            }])

          .controller('mainCtrl', ['$scope', 'authentication',
            function ($scope, authentication) {
              $scope.headerTitle = "Landing Page";
              $scope.isLoggedIn = false;
              $scope.$on('$stateChangeSuccess', function () {
                $scope.isLoggedIn = authentication.isAuthorized();
              });

            }]);


})();